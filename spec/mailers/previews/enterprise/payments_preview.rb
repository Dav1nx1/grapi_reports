# Preview all emails at http://localhost:3000/rails/mailers/enterprise/payments
class Enterprise::PaymentsPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/enterprise/payments/error_save
  def error_save
    Enterprise::Payments.error_save
  end

end
