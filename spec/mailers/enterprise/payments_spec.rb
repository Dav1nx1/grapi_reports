require "rails_helper"

RSpec.describe Enterprise::Payments, type: :mailer do
  describe "error_save" do
    let(:mail) { Enterprise::Payments.error_save }

    it "renders the headers" do
      expect(mail.subject).to eq("Error save")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
