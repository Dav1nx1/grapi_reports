require 'rails_helper'

RSpec.describe Api::V1::Location::CountriesController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
