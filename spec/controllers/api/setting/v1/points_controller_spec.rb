require 'rails_helper'

RSpec.describe API::Setting::V1::PointsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
