class API::OC::Transaccion_Serializer < ActiveModel::Serializer
  attributes :id, :total_last_year, :total_current_year, :centro_id, :tipo_pago
end
