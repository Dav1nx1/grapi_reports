class Setting < ActiveRecord::Base
   belongs_to  :status_entity, -> {self.select_fields}
   belongs_to  :country
   has_many    :commerces, through: :country
end
