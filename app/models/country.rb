# This class is a model used to describe a country properties.
#   string     :code - ISO alpha-2
#   string     :name - Name of the ocuntry

class Country < ActiveRecord::Base

  has_many :states, -> {self.select_fields}

  scope :select_fields, ->{
    self.select(:id, :code, :name).order(:name)
  }

  # Get all countries or filter by an ID
  # id:   Country ID
  def self.get_countries(id = nil)
    @countries = (id) ? self.select_fields.find(id) : self.select_fields
    @code = 200
    @success = true
  end

  def self.countries_exception_not_found
    @countries = {}
    @code = 404
    @success = false
  end

  # Scope Name: countries
  # This scope return a JSON structure with all countries available under API
  # Input values:
  # None
  # Return values:
  # +countries+  all countries serialized with JSON.
  # +success+    boolean value to define is the transaction is success or not.
  # +code+       http code response.
  # +message+    information or error message.
  scope :countries, -> {
    begin
      get_countries
      @message   =  I18n.t('system.countries.messages.success.all_countries')
    rescue Exception => e
      countries_exception_not_found
      @message   =  'Exception: ' + e.message + ' ' + I18n.t('system.countries.messages.error.all_countries')
    end
    { success: @success, message: @message, code: @code, countries: @countries.to_json}
  }

  # This scope return a JSON structure with a particular country available.
  # Input values:
  # id:           Country ID
  # Return values:
  # +countries+   all countries serialized with JSON.
  # +success+     boolean value to define is the transaction is success or not.
  # +code+        http code response.
  # +message+     information or error message.
  scope :get_country_by_id, ->(id) {
    begin
      get_countries(id)
      @message   =  I18n.t('system.countries.messages.success.search_id')
    rescue Exception => e
      countries_exception_not_found
      @message   =  'Exception: ' + e.message + ' ' + I18n.t('system.countries.messages.error.search_id')
    end
    { success: @success, message: @message, code: @code, country: @countries.to_json }
  }

  # This scope return a JSON structure with a particular country available with all states associated.
  # Input values:
  # id:           Country ID
  # Return values:
  # +countries+   all countries and states serialized with JSON.
  # +success+     boolean value to define is the transaction is success or not.
  # +code+        http code response.
  # +message+     information or error message.
  scope :get_country_by_id_with_states, ->(id) {
    begin
      get_countries(id)
      @message   =  I18n.t('system.countries.messages.success.search_with_states_id')
    rescue Exception => e
      countries_exception_not_found
      @message   =  'Exception: ' + e.message + ' ' + I18n.t('system.countries.messages.error.search_with_states_id')
    end
    { success: @success, message: @message, code: @code, country: @countries.to_json(:include => [:states])}
  }
end