class State < ActiveRecord::Base
	
	belongs_to :country

    scope :select_fields, -> {
      self.select(:id, :code_iso_3166_2, :country_id, :name).order(:name)
    }
	
end
