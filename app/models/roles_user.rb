class RolesUser < ActiveRecord::Base
	  belongs_to :user
	  belongs_to :role
	  belongs_to :status_entity, -> {self.select_fields}
end
