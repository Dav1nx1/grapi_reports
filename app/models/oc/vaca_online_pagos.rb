
class OC::VacaOnlinePagos < ActiveRecord::Base
  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.vaca_online_pagos'
end
