class OC::Centro < ActiveRecord::Base
  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.centro'

  #default_scope { select("#{table_name}.id, #{table_name}.audit_created_on, #{table_name}.ciudad_id").
  #    order(:audit_created_on) }

  has_many :oc_canchas, :class_name => 'OC::Cancha', primary_key: 'id', foreign_key: 'centro_id'
  has_one :oc_ciudad, :class_name => 'OC::Ciudad', primary_key: 'ciudad_id', foreign_key: 'id'
  
  has_many :transaccion
  has_many :reservacion

  
  

end
