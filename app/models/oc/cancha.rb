class OC::Cancha < ActiveRecord::Base

  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.cancha'

  belongs_to :oc_centro, :class_name => 'OC::Centro', primary_key: 'id', foreign_key: 'centro_id'

end