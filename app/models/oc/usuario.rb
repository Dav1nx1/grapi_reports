class OC::Usuario < ActiveRecord::Base

  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.usuario'

  attr_accessor :total_last_year, :total_current_year, :total_by_user

  def attributes
     super.merge!(total_last_year: nil, total_current_year: nil, total_by_user: nil)
  end

  has_many :transaccion, -> { self.select(:id, :fecha_transaccion, :usuario_id, :centro_id).includes(:centro).where('fecha_transaccion BETWEEN ? AND ?',"#{Time.now.year}-01-01".to_date - 1.year, Time.now) }
  has_many :reservacion, -> { self.select(:id, :fecha_reservacion, :usuario_id, :centro_id).includes(:centro).where('fecha_reservacion BETWEEN ? AND ?',"#{Time.now.year}-01-01".to_date - 1.year, Time.now) }

  scope :count_month_test, -> {
     users = self.select(:id, :nombre, :cedula).includes(:transaccion, :reservacion).
     where(id: [71241])

     users.each do |user|
       user.serializable_hash
       months_temp = Array.new()
       self.dates.each do |_begin, _end|
         months_temp << user.reservacion.select{|t| (t.fecha_reservacion >= _begin.to_date - 1.year && t.fecha_reservacion <= _end.to_date - 1.year)}.size
       end
       user.total_last_year = months_temp
     end

     users.each do |user|
       user.serializable_hash
       months_temp = Array.new()
       user.total_by_user = user.reservacion.size
       self.dates.each do |_begin, _end|
         months_temp << user.reservacion.select{|t| (t.fecha_reservacion >= _begin && t.fecha_reservacion <= _end)}.size
       end
       user.total_current_year = months_temp
     end

     return users
  }

  scope :count_month, -> {

     users = self.select(:id, :nombre, :cedula).includes(:transaccion, :reservacion)

     users.each do |user|
       months_temp = Array.new()
       self.dates.each do |_begin, _end|
         months_temp << user.reservacion.select{|t| (t.fecha_reservacion >= _begin.to_date - 1.year && t.fecha_reservacion <= _end.to_date - 1.year)}.size
       end
       user.total_last_year = months_temp
     end

     users.each do |user|
       months_temp = Array.new()
       user.total_by_user = user.reservacion.size
       self.dates.each do |_begin, _end|
         months_temp << user.reservacion.select{|t| (t.fecha_reservacion >= _begin && t.fecha_reservacion <= _end)}.size
       end
       user.total_current_year = months_temp
     end

     return users

  }

  private
    def self.dates
       [
        ["#{Time.now.year}-01-01", "#{Time.now.year}-01-31"],
        ["#{Time.now.year}-02-01", "#{Time.now.year}-02-28"],
        ["#{Time.now.year}-03-01", "#{Time.now.year}-03-31"],
        ["#{Time.now.year}-04-01", "#{Time.now.year}-04-30"],
        ["#{Time.now.year}-05-01", "#{Time.now.year}-05-31"],
        ["#{Time.now.year}-06-01", "#{Time.now.year}-06-30"],
        ["#{Time.now.year}-07-01", "#{Time.now.year}-07-31"],
        ["#{Time.now.year}-08-01", "#{Time.now.year}-08-31"],
        ["#{Time.now.year}-09-01", "#{Time.now.year}-09-30"],
        ["#{Time.now.year}-10-01", "#{Time.now.year}-10-31"],
        ["#{Time.now.year}-11-01", "#{Time.now.year}-11-30"],
        ["#{Time.now.year}-12-01", "#{Time.now.year}-12-31"],
       ]
    end

end