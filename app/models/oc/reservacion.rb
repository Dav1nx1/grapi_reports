
class OC::Reservacion < ActiveRecord::Base
  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.reservacion'

  belongs_to :usuario
  belongs_to :centro
  belongs_to :transaccion
  
end
