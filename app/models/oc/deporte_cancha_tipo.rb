class OC::DeporteCanchaTipo < ActiveRecord::Base
  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.deporte_cancha_tipo'
end
