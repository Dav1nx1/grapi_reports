class OC::Ciudad < ActiveRecord::Base
  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.ciudad'

  belongs_to :oc_centro, :class_name => 'OC::Centro'

end
