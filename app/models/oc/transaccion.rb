
# Guia busqueda
# users = self.select(:id, :nombre, :cedula).includes(:transaccion, :reservacion).
     # where(id: [71241])

class OC::Transaccion < ActiveRecord::Base

  include EstadoTransaccion
  include ActiveModel::Serialization

  self.establish_connection eval(":oc_curia_#{Rails.env}")
  self.primary_key = 'id'
  self.table_name = 'public.transaccion'

  attr_accessor :total_last_year, :total_current_year

  def attributes
     super.merge!(total_last_year: nil, total_current_year: nil)
  end

  belongs_to :usuario
  belongs_to :centro

  trans = self.new

  scope :transfers_d, ->{
    transfers = self.select(:id, :centro_id, :fecha_transaccion, :tipo_pago, :total_con_impuestos)

    trans.serializable_hash
    month_temp = Array.new()
    self.tpago.each do |tp|
      temp_tpago = Array.new()
      self.dates.each do |_begin, _end|
        temp_tpago << transfers.select{|t| (t.fecha_transaccion >= _begin.to_date - 1.year && t.fecha_transaccion <= _end.to_date - 1.year && t.tipo_pago == tp )}.size
      end
      month_temp << temp_tpago
    end
    trans.total_last_year = month_temp

    month_temp = Array.new()
    self.tpago.each do |tp|
      temp_tpago = Array.new()
      self.dates.each do |_begin, _end|
        temp_tpago << transfers.select{|t| (t.fecha_transaccion >= _begin.to_date  && t.fecha_transaccion <= _end.to_date && t.tipo_pago == tp )}.size
      end
      month_temp << temp_tpago
    end
    trans.total_current_year = month_temp

    return trans.total_current_year, trans.total_last_year
  }




   # def self.total_last_year
   # self.tpago.each{|tipo_pago| @totales_tipopago[tipo_pago] = self.count_transfers(tipo_pago)}
   # transfers_total_last = @totales_tipopago
   # end





    private
    def self.dates
       [
        ["#{Time.now.year}-01-01", "#{Time.now.year}-01-31"],
        ["#{Time.now.year}-02-01", "#{Time.now.year}-02-28"],
        ["#{Time.now.year}-03-01", "#{Time.now.year}-03-31"],
        ["#{Time.now.year}-04-01", "#{Time.now.year}-04-30"],
        ["#{Time.now.year}-05-01", "#{Time.now.year}-05-31"],
        ["#{Time.now.year}-06-01", "#{Time.now.year}-06-30"],
        ["#{Time.now.year}-07-01", "#{Time.now.year}-07-31"],
        ["#{Time.now.year}-08-01", "#{Time.now.year}-08-31"],
        ["#{Time.now.year}-09-01", "#{Time.now.year}-09-30"],
        ["#{Time.now.year}-10-01", "#{Time.now.year}-10-31"],
        ["#{Time.now.year}-11-01", "#{Time.now.year}-11-30"],
        ["#{Time.now.year}-12-01", "#{Time.now.year}-12-31"],
       ]
    end

    def self.tpago
      [0,1,2,3,4,5,6,7]
    end
end



