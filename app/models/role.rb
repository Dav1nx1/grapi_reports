class Role < ActiveRecord::Base
  belongs_to :status_entity, -> {self.select_fields}
  has_many   :roles_users
  has_many   :users, through: :roles_users

  scope :select_fields, -> {
	self.select(:id, :code, :name).order(:name)
  }

end
