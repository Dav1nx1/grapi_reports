class User < ActiveRecord::Base
  include Common, PaymentsMethods

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  attr_accessor :password_confirmation

  belongs_to :status_entity, -> {self.select_fields}
  has_many :authentication_tokens
  has_many :roles_users
  has_many :roles, -> {self.select_fields}, through: :roles_users
  has_many :payments
  
  validates_presence_of :password, on: :create
  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :user_tag
  validates_presence_of :country
  validates_presence_of :email

  validates_format_of [:first_name, :last_name], :with => /\A([a-zA-Z_áéíóúñ0-9\s]*$)/i
  validates_format_of [:address, :street_name], :with => /\A([a-zA-Z_áéíóúñ0-9#()-.\s]*$)/i, allow_blank: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  validates :home_phone, :mobile_phone, length: { minimum: 7,  maximum: 11, allow_blank: true }
  validates :password, length: { minimum: 8, allow_blank: true}

  validates_numericality_of [:home_phone, :mobile_phone, :street_num, :zip_code], allow_blank: true
  
  validates_uniqueness_of :email
  validates_uniqueness_of :user_tag

  after_rollback :print_error
  
  scope :active_account, ->(token){
      resource = self.find_by(active_token: token)
      if resource
        resource.status_entity_id = StatusEntity.active.id
        resource.active_token     = nil
        resource.save
      end
      return resource
   }

  def is_password?(password)
    BCrypt::Password.new(self.encrypted_password) == password
  end

  def load_status_token
     self.status_entity_id = StatusEntity.inactive.id
     self.active_token     = Digest::MD5.hexdigest("#{Environment::SEED_CREATE_USER}#{Time.now.strftime('%d%m%Y%H%M%S')}")
  end

  def add_role
    RolesUser.create(status_entity_id: StatusEntity.find_by(code: 'ACT').id, user_id: self.id, role_id: Role.find_by(code: 'CUS').id)
  end

  private 
    def print_error
      self.errors.each do |key, value|
         puts "#{key}: #{value}"
      end
    end  

end
