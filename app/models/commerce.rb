class Commerce < ActiveRecord::Base
    include Common
    
    attr_accessor :points_ids

  	belongs_to :user
  	belongs_to :status_entity, -> {self.select_fields}
  	belongs_to :country, -> {self.select_fields}
  	has_one    :setting, through: :country
  	has_many   :commerces_points, -> {self.select_fields}
  	has_many   :points, -> {self.select_fields}, through: :commerces_points
    has_many   :payments
    
    validates_presence_of       :points_ids
    validates_presence_of       :user_id
    validates_presence_of       :country_id
    validates_presence_of       :name
    validates_presence_of       :web

    validates_format_of :web,  with: /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)\z/i
    validates_format_of :logo, with: /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)\z/i, allow_blank: true
    validates_format_of :name, :with => /\A([0-9a-zA-Z_áéíóúñ&\s]*$)/i 
    validates_format_of :note, :with => /\A([0-9a-zA-Z_áéíóúñ\s]*$)/i, allow_blank: true
    
    validates :mobile_phone, length: { minimum: 7,  maximum: 11}, allow_blank: true
    validates :local_phone, length: { minimum: 7,  maximum: 11}, allow_blank: true
    validates :logo, length: { maximum: 200}, allow_blank: true
    validates :note, length: { minimum: 10,  maximum: 300}, allow_blank: true
    validates :name, length: { minimum: 5,  maximum: 50}
    validates :web, length:  {maximum: 100 }

    validates_uniqueness_of   :web
    validates_uniqueness_of   :name
    validates_numericality_of :mobile_phone, allow_blank: true
    validates_numericality_of :local_phone, allow_blank: true

    before_create :create_business_rules, :create_key
    before_update :create_business_rules
    after_create  :create_points
    after_update  :create_points

    scope :select_fields, ->{
      self.select(:id, :name, :logo).order(:name)
    }

    scope :list_commerces_admin, ->{
      self.where.not(status_entity_id: StatusEntity.deleted).includes(:country)
    }

    scope :list_commerces, ->(user){
      self.where(user_id: user.id).where.not(status_entity_id: StatusEntity.deleted).includes(:country)
    }

    private
        def create_business_rules
          rules = Array.new()
          rules.push(exists_setting)
          rules.each do |rule|
            return false if rule == false
          end
          return true
        end

        def exists_setting
           setting = Setting.find_by(country_id: self.country_id)
           if setting
             self.setting_id = setting.id
             return true
           else
             errors.add(:exists_setting,  I18n.t('system.commerces.messages.error.exists_setting')); false
           end
        end

        def create_key
            begin
              self.public_key       = Digest::MD5.hexdigest("#{self.name}#{Time.now.strftime('%d%m%Y%H%M%S')}")
              self.status_entity_id = StatusEntity.active.id
              self.created_by       = self.user_id
              self.updated_by       = self.user_id  
              return true        
            rescue Exception => e
              errors.add(:create_key,  e.to_s); false
            end 
        end

        def create_points
            CommercesPoint.destroy_all(commerce_id: self.id)
            points = Point.where(id: self.points_ids, status_entity_id: StatusEntity.active.id)
            points.each do |point|
              secret_key  = Digest::MD5.hexdigest("#{point.id}#{point.name}#{point.short_name}#{Time.now.strftime('%d%m%Y%H%M%S')}")
              CommercesPoint.create(point_id: point.id, commerce_id: self.id, status_entity_id: point.status_entity_id, secret_key: secret_key)
            end
        end

end
