class CommercesPoint < ActiveRecord::Base
	belongs_to :commerce
	belongs_to :point, -> {self.select_fields}
	belongs_to :status_entity, -> {self.select_fields}

	scope :select_fields, ->{
      self.select(:id, :secret_key, :commerce_id, :point_id)
    }

end
