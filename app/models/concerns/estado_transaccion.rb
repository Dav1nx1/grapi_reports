module EstadoTransaccion
  extend ActiveSupport::Concern
=begin
  enum estadoTransaccion: [ :pagada, :pago_pendiente, :anulada, :abonada, :deposito_sin_confirmar, :deposito_sin_confirmar_abonado ]
=end

  included do
    scope :pagada, -> {
      where(estado: 0)
    }

    scope :pago_pendiente, -> {
      where(estado: 1)
    }

    scope :anulada, -> {
      where(estado: 2)
    }

    scope :abonada, -> {
      where(estado: 3)
    }

    scope :deposito_sin_confirmar, -> {
      where(estado: 4)
    }

    scope :deposito_sin_confirmar_abonado, -> {
      where(estado: 5)
    }

    

    scope :order_between_dates, -> (start_date, end_date) {
      where(audit_updated_on: start_date..end_date)
    }
  end
end