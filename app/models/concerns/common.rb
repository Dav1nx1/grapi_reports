module Common
  extend ActiveSupport::Concern
  
  def obj_errors
    array = []
    self.errors.each do |key, value|
       array.push "#{key}: #{value}"
    end
    return array
 end

end