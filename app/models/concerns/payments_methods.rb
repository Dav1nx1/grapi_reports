module PaymentsMethods
   extend ActiveSupport::Concern
   
   def insta_pay
        begin
	        url_pay        = self.point.url_pay
	        codification   = 'application/x-www-form-urlencoded'
	        public_key     = self.point.internal_public_key
			secret_key     = self.point.internal_secret_key
	        card_holder    = self.card_holder
	        amount         = self.amount
	        cardHolderId   = self.card_holder_id
	        cardNumber     = self.card_number
	        orderNumber    = self.order_number
	        expirationDate = self.expiration_date
	        code           = self.security_code
	        address        = self.address
	        city           = self.city
	        zipCode        = self.zip_code
	        state          = self.state
	        description    = self.description
	        
	        params       = "KeyId=#{secret_key}&PublicKeyId=#{public_key}&Amount=#{amount}&Description=#{description}&CardHolder=#{card_holder}&CardHolderId=#{cardHolderId}&CardNumber=#{cardNumber}&CVC=#{code}&ExpirationDate=#{expirationDate}&StatusId=2&IP=127.0.0.1&OrderNumber=#{orderNumber}&Address=#{address}&City=#{city}&ZipCode=#{zipCode}&State=#{state}"
	        uri          = URI(url_pay)
	        transaction  = {}

	        Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme) do |http|
	           request      = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => codification })
	           request.body = params
	           response     = http.request request # Net::HTTPResponse object
	           result       = response.body
	           Rails.logger.info('************Info de la transacción con instapagos************')
               Rails.logger.info("#{Time.now}")
               Rails.logger.info(result)
               Rails.logger.error('************************************************************************')    
	           transaction  = JSON.parse(result).deep_symbolize_keys!()
	        end

	        if transaction[:success]
	           self.status_entity_id = StatusEntity.processed.id
               self.code             = Constant::SUCCESS
	        else
	           self.status_entity_id = StatusEntity.rejected.id
               self.code             = Constant::ERROR
	        end

            self.transaction_id   = transaction[:id]
            self.reference        = transaction[:reference]
            self.card_number      = (self.card_number.to_s[-4..self.card_number.to_s.length]).to_i #obtenemos los 4 ultimos de la tarjeta
            self.result           = transaction[:voucher]
            self.responsecode     = transaction[:responsecode]
            self.message          = transaction[:message] 
            #self.address          = nil #descomentar si desea probar el error de la contigencia
            return true
	              	
        rescue Exception => e
        	errors.add(:error_payment, e.to_s)
        	return false
        end
   end


end