class Payment < ActiveRecord::Base
    include Common, PaymentsMethods

	attr_accessor :public_key, :secret_key, :subdomain, :format, :controller, :action,  :security_code ,:expiration_date
	
	belongs_to :commerce, -> {self.select_fields}
	belongs_to :point, -> {self.select_fields}
	belongs_to :status_entity, -> {self.select_fields}
	belongs_to :user
    
  validates_presence_of :public_key
  validates_presence_of :secret_key
  validates_presence_of :card_holder
  validates_presence_of :email
  validates_presence_of :amount
  validates_presence_of :card_number 
  validates_presence_of :security_code
  validates_presence_of :order_number
  validates_presence_of :description
  validates_presence_of :card_holder_id
  validates_presence_of :expiration_date

  validates_format_of :card_holder, :with => /\A([a-zA-Z_áéíóúñ\s]*$)/i 
  validates_format_of :description, :with => /\A([0-9a-zA-Z_áéíóúñ\s]*$)/i
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  
  validates :card_holder,   length: { minimum: 5,  maximum: 50}
  validates :email,  length: {maximum: 50}
  validates :description, length: { minimum: 10,  maximum: 150}
  
  validates_numericality_of :amount
  validates_numericality_of :card_holder_id
  validates_numericality_of :card_number
  validates_numericality_of :security_code

  before_save :set_pay_api

  scope :select_fields, -> {
     self.select( :id, :card_holder, :status_entity_id, :email, :amount, :order_number, :description, :reference, :result)
  }

  scope :list_payments_admin, ->(order_date){
     
     current_date  = Time.parse(Time.now.strftime("%Y-%m-%d"))
     current_month = Date.parse(Time.now.strftime("%B"))
     current_year  = "#{Time.now.year }-01-01 00:00:00"
     start_week    = Date.parse("Monday")
     end_week      = Date.parse("Sunday")
     last_year     = "#{Time.now.year - 1}-01-01 00:00:00"
     query         = "created_at BETWEEN ? AND ?"
     case order_date.to_i
       when 1 # today
        self.includes(:commerce, :point, :status_entity).where(query, current_date.strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59"))
       when 2 # yesterday
        self.includes(:commerce, :point, :status_entity).where(query, (current_date - 1.day).strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59"))
       when 3 # this week
        self.includes(:commerce, :point, :status_entity).where(query, start_week.strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59"))
       when 4 # last week
        self.includes(:commerce, :point, :status_entity).where(query, (start_week - 1.week).strftime("%Y-%m-%d 00:00:00"), end_week.strftime("%Y-%m-%d 23:59:59"))
       when 5 # this month
        self.includes(:commerce, :point, :status_entity).where(query, current_month.strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59")) 
       when 6 # last month
        self.includes(:commerce, :point, :status_entity).where(query, (current_month - 1.month).strftime("%Y-%m-%d 00:00:00"), (current_month - 1.day).strftime("%Y-%m-%d 23:59:59"))
       when 7 # this year
        self.includes(:commerce, :point, :status_entity).where(query, current_year, current_date.strftime("%Y-%m-%d 23:59:59"))
       when 8 # last year
        self.includes(:commerce, :point, :status_entity).where(query, last_year, current_year)
       else # all time
        self.includes(:commerce, :point, :status_entity)
     end

  }

  scope :list_payments, ->(user, order_date){
     
     current_date  = Time.parse(Time.now.strftime("%Y-%m-%d"))
     current_month = Date.parse(Time.now.strftime("%B"))
     current_year  = "#{Time.now.year }-01-01 00:00:00"
     start_week    = Date.parse("Monday")
     end_week      = Date.parse("Sunday")
     last_year     = "#{Time.now.year - 1}-01-01 00:00:00"
     query         = "user_id = ? AND created_at BETWEEN ? AND ?"
     case order_date.to_i
       when 1 # today
        self.includes(:commerce, :point, :status_entity).where(query, user.id ,current_date.strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59"))
       when 2 # yesterday
        self.includes(:commerce, :point, :status_entity).where(query, user.id,(current_date - 1.day).strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59"))
       when 3 # this week
        self.includes(:commerce, :point, :status_entity).where(query, user.id,start_week.strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59"))
       when 4 # last week
        self.includes(:commerce, :point, :status_entity).where(query, user.id,(start_week - 1.week).strftime("%Y-%m-%d 00:00:00"), end_week.strftime("%Y-%m-%d 23:59:59"))
       when 5 # this month
        self.includes(:commerce, :point, :status_entity).where(query, user.id,current_month.strftime("%Y-%m-%d 00:00:00"), current_date.strftime("%Y-%m-%d 23:59:59")) 
       when 6 # last month
        self.includes(:commerce, :point, :status_entity).where(query, user.id,(current_month - 1.month).strftime("%Y-%m-%d 00:00:00"), (current_month - 1.day).strftime("%Y-%m-%d 23:59:59"))
       when 7 # this year
        self.includes(:commerce, :point, :status_entity).where(query, user.id,current_year, current_date.strftime("%Y-%m-%d 23:59:59"))
       when 8 # last year
        self.includes(:commerce, :point, :status_entity).where(query, user.id,last_year, current_year)
       else # all time
        self.includes(:commerce, :point, :status_entity).where(user_id: user.id)
     end
  }

  scope :load_payment, ->(id){
    self.includes(:commerce,:point, :status_entity).find(id)
  }
  
=begin
debera buscar en la tabla payments la transaccion su condicion 
seria where commerce_id = x and order_num = xxx and status_entity_id <> 'procesado' 
el resultado sera evaluado si consiguio una transaccion devolvera el result no sin antes asignar
al result los siguientes attributes:
result.public_key = object.public_key
result.secret_key = object.secret_key
si no consigue nada devolvera el objeto que viene por parametro.
=end
  scope :search_transaction, ->(object){
    
    commerce    = Commerce.find_by(public_key: object.public_key)
    transaction = Payment.where(commerce_id: commerce.id, 
                          order_number: object.order_number)
    if transaction.size > 0   
      transaction.first.public_key      = object.public_key
      transaction.first.secret_key      = object.secret_key
      transaction.first.security_code   = object.security_code
      transaction.first.expiration_date = object.expiration_date
      transaction.first.card_number     = object.card_number
      transaction.first.card_holder     = object.card_holder
      transaction.first.email           = object.email
      transaction.first.card_holder_id  = object.card_holder_id
      transaction.first.address         = object.address
      transaction.first.city            = object.city
      transaction.first.zip_code        = object.zip_code
      transaction.first.state           = object.state
      transaction.first.card_name       = object.card_name
      transaction.first.description     = object.description
      transaction.first.amount          = object.amount
      return transaction.first
    else
      return object
    end
  
  }

  private
    def set_pay_api
      begin
          puts "**********self.card_number*******#{self.card_number}**********************"
          commerce              = Commerce.find_by(public_key: self.public_key)
          point_comm            = CommercesPoint.find_by(secret_key: self.secret_key, commerce_id: commerce.id)
          point                 = Point.find point_comm.point_id
          self.status_entity_id = StatusEntity.standby.id
          self.commerce_id      = commerce.id
          self.user_id          = commerce.user_id
          self.point_id         = point.id
          self.point            = point
          eval point.method_pay
      rescue Exception => e
          errors.add(:error_payment, I18n.t('system.payments.messages.errors.create_auth'));false
      end
    end

end
