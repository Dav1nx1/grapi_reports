class Point < ActiveRecord::Base
	
	 belongs_to :status_entity, -> {self.select_fields}
	 has_many   :commerces_points, -> {self.select_fields}
     has_many   :commerces, through: :commerces_points
     has_many   :payments
     
     scope :select_fields, ->{
      self.select(:id, :short_name, :name).order(:name)
     }

	 # Get all Points or filter by an ID
	  # id:   Point ID
	  def self.get_points(id = nil)
	    @points  = (id) ? self.select_fields.find(id) : self.select_fields.where(status_entity_id: StatusEntity.active.id)
	    @code    = 200
	    @success = true
	  end

	  def self.points_exception_not_found
	    @points  = {}
	    @code    = 404
	    @success = false
	  end

	  # Scope Name: points
	  # This scope return a JSON structure with all points available under API
	  # Input values:
	  # None
	  # Return values:
	  # +points+     all points serialized with JSON.
	  # +success+    boolean value to define is the transaction is success or not.
	  # +code+       http code response.
	  # +message+    information or error message.
	  scope :points, -> {
	    begin
	      get_points
	      @message   =  I18n.t('system.points.messages.success.all_points')
	    rescue Exception => e
	      points_exception_not_found
	      @message   =  'Exception: ' + e.message + ' ' + I18n.t('system.points.messages.error.all_points')
	    end
	    { success: @success, message: @message, code: @code, points: @points.to_json}
	  }

end
