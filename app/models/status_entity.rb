class StatusEntity < ActiveRecord::Base
	has_many :countries
	has_many :points
	has_many :roles
	has_many :users
	has_many :settings
	has_many :roles_users
	has_many :commerces
	has_many :commerces_points

   self.select(:name).uniq().each do |action|
	    scope eval(":#{action.name.downcase}"), -> {
	      select("id").find_by(name: action.name)
	    }
   end

   scope :select_fields, ->{
    self.select(:id, :code, :name).order(:name)
   }
end
