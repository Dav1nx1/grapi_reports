class API::Profile::V1::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]
  # GET /resource/sign_in

  def new
    super
  end

  def create
     email    = params[:email]
     password = params[:password]
     resource = User.find_by(email: email, status_entity_id: StatusEntity.active.id)
     if resource && resource.is_password?(password)
       body_token = Digest::MD5.hexdigest("#{resource.id}#{resource.email}#{Time.now.strftime('%d%m%Y%H%M%S')}")
       authToken  = AuthenticationToken.create(body: body_token, user_id: resource.id, user_agent: 'grapi', last_used_at: Time.now)
       sign_in :user, resource
       render json: {code: 200, authentication_token: authToken.body, user: resource.to_json(:include => :roles),  message: I18n.t('system.login.messages.success.create') }, status: 200
     else
       render json: {code: 401, authentication_token: nil, user: nil,  message: I18n.t('system.login.messages.error.create')}, status: 401
     end
  end

  def destroy
    begin
     Rails.logger.error("verificando token enviado paracierre de session*************** #{params[:token]}")
     auth = AuthenticationToken.find_by(body: params[:token])
     auth.destroy
     render json: {code: 200, authentication_token: nil, user: nil,  message: I18n.t('system.login.messages.success.destroy') }, status: 200
    rescue Exception => e
      Rails.logger.error('************Error al intentar cerrar session en production destroy************')
      Rails.logger.error("#{Time.now}")
      Rails.logger.error("Error: ---- #{e.to_s}")
      Rails.logger.error('************************************************************************')
      render json: {code: 200, authentication_token: nil, user: nil,  message: I18n.t('system.login.messages.error.destroy_error') }, status: 401     
    end
  end


  private
    # this is invoked before destroy and we have to override it
    def verify_signed_out_user
    end

 # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end

end
