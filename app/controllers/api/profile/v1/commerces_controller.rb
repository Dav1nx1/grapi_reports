class API::Profile::V1::CommercesController < ApplicationController
  before_filter :auth_user
  before_filter :load_commerce, only: [:edit, :show, :update, :destroy]
=begin
    /profile/v1/commerces           index   get    --> only admin
    /profile/v1/commerces           create  post   --> all
    /profile/v1/commerces/new               get    --> all
    /profile/v1/commerces/:id/edit          get    --> all
    /profile/v1/commerces/:id       show    get    --> all
    /profile/v1/commerces/:id       update  path   --> all
    /profile/v1/commerces/:id       update  put    --> all
    /profile/v1/commerces/:id       destroy delete --> only admin
=end
  	def index
      if self.roles Environment::CODE_ADMIN
  	    @commerces = Commerce.list_commerces_admin
  	  else
        @commerces = Commerce.list_commerces current_user
      end
      render json: {commerces: @commerces.to_json(:include => :country), code: 200, message: I18n.t('system.commerces.messages.success.list')}, status: 200
    end

    def new
    	
    end

    def create
      @commerce  = Commerce.new allowed_params
      if @commerce.save
        render json: {commerce: @commerce.to_json(:include => {:commerces_points => {:include => :point}, :country => {}}), code: 201, message: I18n.t('system.commerces.messages.success.create')}, status: 201
      else
        render json: {commerce: @commerce.to_json, code: 422, errors: @commerce.obj_errors.to_json}, status: 422
      end
    end

    def edit
      begin
        render json: {commerce: @commerce.to_json(:include => {:commerces_points => {:include => :point}, :country => {}}), code: 200, message: I18n.t('system.commerces.messages.success.edit')}, status: 200  
      rescue Exception => e
        render json: {code: 422, errors: [e.to_s].to_json,  message: I18n.t('system.commerces.messages.error.show')}, status: 422; return
      end
    end

    def update
      begin
        if @commerce.update(allowed_params)
          render json: {commerce: @commerce.to_json(:include => {:commerces_points => {:include => :point}, :country => {}}), code: 201, message: I18n.t('system.commerces.messages.success.update')}, status: 201
        else
          render json: {commerce: @commerce.to_json, code: 422, errors: @commerce.obj_errors.to_json}, status: 422
        end        
      rescue Exception => e
        render json: {commerce: @commerce.to_json, code: 422, errors: [e.to_s].to_json}, status: 422
      end
    end

    def show
      begin
        render json: {commerce: @commerce.to_json(:include => {:commerces_points => {:include => :point}, :country => {}}), code: 200, message: nil}, status: 200
      rescue Exception => e
        render json: {code: 422, errors: [e.to_s].to_json,  message: I18n.t('system.commerces.messages.error.show')}, status: 422; return
      end
    end

    def destroy
      begin
        status_id = StatusEntity.deleted.id
        if @commerce.update_attributes(status_entity_id: status_id)
          render json: {code: 200, message: I18n.t('system.commerces.messages.success.delete')}, status: 200
        else
          render json: {code: 403, errors: @commerce.obj_errors.to_json , message: I18n.t('system.commerces.messages.error.delete_auth')}, status: 403
        end        
      rescue Exception => e
        render json: {code: 422, errors: [e.to_s] , message: I18n.t('system.commerces.messages.error.delete_auth')}, status: 422
      end
    end

    private
      def allowed_params
        params.require(:commerce).permit!
      end

      def load_commerce
        id = params[:id]
        if self.roles Environment::CODE_ADMIN
          @commerce =  Commerce.find_by(id: id)
        else
          @commerce =  Commerce.find_by(id: id, user_id: current_user.id)
          unless @commerce
            render json: {code: 404, errors: [I18n.t('system.commerces.messages.error.auth_user')].to_json,  message: I18n.t('system.commerces.messages.error.auth_user')}, status: 404; return
          end
        end
      end
end
