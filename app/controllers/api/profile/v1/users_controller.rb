class API::Profile::V1::UsersController < ApplicationController
   
   before_filter :auth_user, :load_user, only: [:edit, :update]
   before_filter :confirmation_password, only: [:create ,:change_pass]
   
   def create
     resource = User.new allowed_params
     resource.load_status_token #load active token and status inactive
     if resource.save
       resource.add_role
       render json: {code: 201, user: resource.to_json, message: I18n.t('system.user.messages.success.create') }, status: 201
     else
       render json: {user: resource.to_json, code: 400, errors: resource.obj_errors.to_json}, status: 400
     end
   end

   def edit
     render json: {code: 200, user: @user.to_json(:include => :roles),  message: nil }, status: 200
   end

   def update
     begin     
        if @user.update(allowed_params)
          render json: {user: @user.to_json(:include => :roles), code: 201, message: I18n.t('system.user.messages.success.update')}, status: 201
        else
          render json: {user: @user.to_json, code: 422, errors: @user.obj_errors.to_json}, status: 422
        end        
      rescue Exception => e
        render json: {user: @user.to_json, code: 422, errors: [e.to_s].to_json}, status: 422
      end
   end

   def active_account
     begin
        active_token = params[:active_token]
        resource     = User.active_account(active_token)
        body_token   = Digest::MD5.hexdigest("#{resource.id}#{resource.email}#{Time.now.strftime('%d%m%Y%H%M%S')}")
        authToken    = AuthenticationToken.create(body: body_token, user_id: resource.id, user_agent: 'grapi', last_used_at: Time.now)
        sign_in :user, resource
        render json: {code: 200, authentication_token: authToken.body, user: resource.to_json(:include => :roles),  message: I18n.t('system.login.messages.success.create') }, status: 200
     rescue Exception => e
        render json: {code: 401, authentication_token: nil, user: nil,  message: I18n.t('system.user.messages.error.active_account')}, status: 401
     end
   end

   #post /profile/v1/users/forgot_pass
   def forgot_pass
      begin
        email     =  params[:user][:email]
        resource  =  User.find_by_email(email)
        if resource
          token_pass          = Digest::MD5.hexdigest("#{resource.id}#{resource.email}#{Time.now.strftime('%d%m%Y%H%M%S')}") 
          resource.token_pass = token_pass
          resource.save
          render json: {code: 201, user: resource.to_json,  message: I18n.t('system.user.messages.success.create_token') }, status: 201
        else
          render json: {code: 401, errors: [I18n.t('system.user.messages.error.create_token')].to_json,  message: I18n.t('system.user.messages.error.create_token') }, status: 401
        end        
      rescue Exception => e
        render json: {code: 401, errors: [e.to_s].to_json,  message: I18n.t('system.user.messages.error.create_token') }, status: 401
      end
   end

   #post /profile/v1/users/change_pass/:token_pass
   def change_pass
     begin
       token_pass =  params[:token_pass]
       resource   =  User.find_by(token_pass: token_pass)
       if resource
         user = User.new allowed_params
         resource.password   = user.password
         resource.token_pass = nil
         if resource.save
           render json: {code: 201,  message: I18n.t('system.user.messages.success.change_pass') }, status: 201
         else
           render json: {code: 422, errors: resource.obj_errors.to_json}, status: 422
         end
       else
         render json: {code: 401, errors: [I18n.t('system.user.messages.error.change_pass')].to_json,  message: I18n.t('system.user.messages.error.change_pass') }, status: 401
       end       
     rescue Exception => e
        render json: {code: 401, errors: [e.to_s].to_json,  message: I18n.t('system.user.messages.error.change_pass') }, status: 401
     end
   end

  private
    def allowed_params
      params.require(:user).permit!
    end

    def confirmation_password
      unless (params[:user][:password] == params[:user][:password_confirmation])
        render json: {user: params[:user].to_json, code: 404, errors: [I18n.t('system.user.messages.error.password')].to_json}, status: 404
        return
      end
    end

    def load_user
        if self.roles Environment::CODE_ADMIN
          id    = params[:id]
          @user = User.find(params[:id])
        else
          @user = current_user
        end
    end

end
