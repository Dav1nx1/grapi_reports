class API::TestPaymentsController < ApplicationController
	def insta_pay

        url_pay      = 'https://api.instapago.com/payment'
        codification = 'application/x-www-form-urlencoded'

        public_key   = '1783b25bca451cc74f90a776838ab242'
		    secret_key   = '1B2398F2-1B8C-4BA2-B3BF-A779F589D77E'
        name_user    = 'Mario+Galvez'
        amount       = '18000.00'
        cardHolder   = '12345671'
        cardNumber   = '5105105105105100'
        orderNumber  = '000000013'
        address      = 'Ave+Las+Palmas+Casa+12'
        city         = 'Ciudad+San+Cristobal'
        zipCode      = '5001'
        state        = 'Tachira'
        code         = "123"

        params       = "KeyId=#{secret_key}&PublicKeyId=#{public_key}&Amount=#{amount}&Description=Pago+Servicios+de+InstaPago&CardHolder=#{name_user}&CardHolderId=#{cardHolder}&CardNumber=#{cardNumber}&CVC=#{code}&ExpirationDate=10%2F2021&StatusId=2&IP=127.0.0.1&OrderNumber=#{orderNumber}&Address=#{address}&City=#{city}&ZipCode=#{zipCode}&State=#{state}"
        uri          = URI(url_pay)

        Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme) do |http|
           request      = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => codification })
           request.body = params
           response     = http.request request # Net::HTTPResponse object
           result       = response.body
           data         = JSON.parse(result)
           render json: data, status: 200
        end
	end


  def total_transactions
    users = ::OC::Usuario.count_month_test
    render json: {report: users.to_json(:include => {:transaccion => {:include => :centro}, :reservacion => {:include => :centro}}), code: 200, message: 'Ok se ejecuto' }, status: 200
  end

  def total_center
    transaction = ::OC::Transaccion.transfers_d
    render json: {report: transaction.to_json(), code: 200, message: 'Ok se ejecuto' }, status: 200
  end

end