class API::Human::V1::NamesController < ApplicationController

  def random_complete_name
    begin
      @name = Faker::Name.first_name + ' ' + Faker::Name.last_name
      response = @name
    rescue => e
      @success = false
      @code = 501
      @message = 'Exception Error:' + e.message + ' - ' + I18n.t('system.human.messages.success.fail')
      response = {success: @success, code: @code, message: @message}
    end
    response
  end

  def index
    response = random_complete_name

    render json: response.to_json, status: :ok
  end

  def show
    @names = Array.new()

    params[:id].to_i.times do
      @names.push(random_complete_name)
    end

    render json: @names.to_json, status: :ok
  end
end