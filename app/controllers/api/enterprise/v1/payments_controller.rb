class API::Enterprise::V1::PaymentsController < ApplicationController
	before_filter :auth_user, only: [:index, :show]
  before_filter :load_payment, only: [:show]

  # /enterprise/v1/payments
  #/enterprise/v1/payments
  #/enterprise/v1/payments/:id
  def index
    filter = {statusEntity: StatusEntity.select_fields.where(code: ['PROCD', 'REJD'])}
    if self.roles Environment::CODE_ADMIN
      search   = Payment.list_payments_admin(params[:order_date]).search(eval(params[:q]))
      payments = search.result.page(params[:page]).per(15)
      filter.store(:commerces, Commerce.select_fields)
    else
      search   = Payment.list_payments(current_user, params[:order_date]).search(eval(params[:q]))
      payments = search.result.page(params[:page]).per(15)
      filter.store(:commerces, Commerce.select_fields.where(user_id: current_user))
    end
    render json: {payments: payments.to_json(:include => {:commerce => {}, :point => {}, :status_entity => {}}), code: 200, message: I18n.t('system.payments.messages.success.list'), filter: filter.to_json ,pagination: {total: payments.total_count, total_pages: payments.total_pages, page: payments.current_page}}, status: 200
  end

	def create
   
    begin
      
      begin
        #@payment = Payment.search_transaction(Payment.new(allowed_params)) este valida que no se repitan transacciones para el mismo comercio
        @payment = Payment.new(allowed_params)
      rescue Exception => e
        #se valida si algun parametro no esta permido antes de continuar con la operacion
        render json: {payment: @payment.to_json, code: 403, errors: [e.to_s].to_json }, status: 403
        return
      end
      
      if @payment.save
        @payment = Payment.load_payment(@payment.id)
        render json: {payment: @payment.to_json(:include => {:commerce => {}, :point => {}, :status_entity => {}}), code: @payment.code, message: @payment.message}, status: 201
      else
        render json: {payment: @payment.to_json, code: 404, errors: @payment.obj_errors.to_json}, status: 404
      end

    rescue Exception => e
      #esta exepcion siempre se va acapturar postre a la respuesta de la pasarela de 
      #pago osea cuando se va a salar la transaccion en grapi si ocurre algun error entra aca
      send_error_to_staff(e)
      render json: {payment: @payment.to_json, code: 201, message: I18n.t('system.payments.messages.success.create')}, status: 201
    end

	end

  def show
      begin
        render json: {payment: @payment.to_json(:include => {:commerce => {}, :point => {}, :status_entity => {}}), code: 200, message: nil}, status: 200
      rescue Exception => e
        render json: {code: 422, errors: [e.to_s].to_json,  message: I18n.t('system.payments.messages.errors.auth_user')}, status: 422
      end  
  end

  def transaction
      begin
        @commerce = Commerce.find_by(public_key: params[:public_key])
        @payment  = Payment.select_fields.includes(:status_entity).where(order_number: params[:order_num], commerce_id: @commerce.id).order(:id).last
        if @payment
          render json: {payment: @payment.to_json(:include => {:status_entity => {}}), code: 200, message: nil}, status: 200
        else
          render json: {code: 404, errors: [I18n.t('system.payments.messages.errors.transaction')].to_json,  message: I18n.t('system.payments.messages.errors.transaction')}, status: 404
        end
      rescue Exception => e
        render json: {code: 422, errors: [e.to_s].to_json,  message: I18n.t('system.payments.messages.errors.auth_user')}, status: 422
      end
  end

  private
    def allowed_params
      params_pay = ActionController::Parameters.new({payment: params})
      params_pay.require(:payment).permit!
    end

    def load_payment
      id = params[:id]
      if self.roles Environment::CODE_ADMIN
        @payment =  Payment.find_by(id: id)
      else
        @payment =  Payment.find_by(id: id, user_id: current_user.id)
        unless @payment
          render json: {code: 404, errors: [I18n.t('system.payments.messages.errors.auth_user')].to_json,  message: I18n.t('system.payments.messages.errors.auth_user')}, status: 404; return
        end
      end
    end

    def send_error_to_staff(e)
      Rails.logger.error('************Error al intentar salvar la transacción en grapi************')
      Rails.logger.error("#{Time.now}")
      Rails.logger.error("Error: ---- #{e.to_s}")
      Rails.logger.error("Transaccion: ----  #{@payment.to_json}")
      Rails.logger.error('************************************************************************')
      PaymentMailer.error_save(e.to_s, @payment.to_json).deliver_now!
    end


end
