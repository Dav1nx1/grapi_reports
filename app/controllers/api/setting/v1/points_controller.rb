class API::Setting::V1::PointsController < ApplicationController

  def index
  	#/setting/v1/points
    @points = Point.points
  	render json: @points, status: @points[:code]
  end
end
