class API::Address::V1::StatesController < ApplicationController
  #address/v1/countries/:country_id/states

  def index
    country_id = params[:country_id]
    @country   = Country.get_country_by_id_with_states(country_id)
    render json: @country, status: @country[:code]
  end

end
