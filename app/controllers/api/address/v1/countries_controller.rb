class API::Address::V1::CountriesController < ApplicationController

  def index
  	#address/v1/countries
    #puts response.headers
    #puts "****************************"
    @countries = Country.countries
  	render json: @countries, status: @countries[:code]
  end

  def show
  	#address/v1/countries/:id
  	id       = params[:id]
    @country = Country.get_country_by_id(id)
  	render json: @country, status: @country[:code]
  end

end
