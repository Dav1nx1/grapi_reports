class API::OC::V1::CentrosController < ApplicationController
  def index
    @centros = ::OC::Centro.all.includes(:oc_ciudad)

    render json: @centros.to_json, status: :ok
  end

  def show
    @centro = ::OC::Centro.find(params[:id])

    render json: @centro.to_json, status: :ok
  end
end