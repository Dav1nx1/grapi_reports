class API::OC::V1::TransaccionesController < ApplicationController

  def get_data(q = ::OC::Transaccion.all)
    data = Hash.new
    data[:data] = q
    data[:metadata] = Hash.new
    data[:metadata][:json_version] = Environment::JSON_VERSION_V1
    data[:stadistics] = Hash.new
    data[:stadistics][:total] = q.count
    data[:stadistics][:pagada] = q.pagada.count
    data[:stadistics][:pago_pendiente] = q.pago_pendiente.count
    data[:stadistics][:anulada] = q.anulada.count
    data[:stadistics][:abonada] = q.abonada.count
    data[:stadistics][:deposito_sin_confirmar] = q.deposito_sin_confirmar.count
    data[:stadistics][:deposito_sin_confirmar_abonado] = q.deposito_sin_confirmar_abonado.count

    data
  end

  def index
    #TODO: Filter based on user role
    @transactions = get_data

    render json: @transactions.to_json, status: :ok
  end

  def show
    #TODO: Filter based on user role
    @transactions = get_data(::OC::Transaccion.find(params[:id]))

    render json: @transactions.to_json, status: :ok
  end

  def today
    today = Time.zone.now-3.days # 3 days should be removed this is only for development proposes

    @transactions = get_data(::OC::Transaccion.order_between_dates(today.beginning_of_day, today))

    render json: @transactions.to_json, status: :ok
  end

  def yesterday
    yesterday = Time.zone.now - 1.day
    @transactions = ::OC::Transaccion.order_between_dates(yesterday.beginning_of_day, yesterday)

    render json: @transactions.to_json, status: :ok
  end

  def by_sport_center
    @transactions = ::OC::Transaccion.where(centro_id: params[:sport_center])

    render json: @transactions.to_json, status: :ok
  end

end
