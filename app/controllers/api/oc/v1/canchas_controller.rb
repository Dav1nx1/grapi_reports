class API::OC::V1::CanchasController < ApplicationController

  def index
    @canchas = ::OC::Cancha.all

    render json: @canchas.to_json, status: :ok
  end

  def show
    @cancha = ::OC::Cancha.find(params[:id])

    render json: @cancha.to_json, status: :ok
  end
end
