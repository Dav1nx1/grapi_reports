class API::Math::V1::NumbersController < ApplicationController

  def integer(size = 5, quantity = 1)

    size = params[:size].to_i if params[:size]
    quantity = params[:quantity].to_i if params[:quantity]

    @numbers = []

    quantity.to_i.times do
      @numbers.push(@number = Faker::Number.number(size))
    end

    render json: @numbers.to_json, status: :ok
  end

  def decimal(size = '5,2', quantity = 1)
    size = params[:size] if params[:size]
    size = size.to_s.split(/\s*[,]\s*/)
    quantity = params[:quantity].to_i if params[:quantity]

    @numbers = []

    quantity.to_i.times do
      @numbers.push(@number = Faker::Number.decimal(size[0].to_i, size[1].to_i))
    end

    render json: @numbers.to_json, status: :ok
  end

  def digit
    render json: Faker::Number.digit.to_json, status: :ok
  end

  def hexadecimal(size = 5, quantity = 1)
    @numbers = []

    size = params[:size].to_i if params[:size]
    quantity = params[:quantity].to_i if params[:quantity]

    quantity.to_i.times do
      @numbers.push(@number = Faker::Number.hexadecimal(size))
    end

    render json: @numbers.to_json, status: :ok
  end
end