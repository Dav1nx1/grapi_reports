module CurrentMethods
   extend ActiveSupport::Concern
   
   def current_user=(val)
     session[:user] = val
   end

   def current_user
	   return session[:user]
   end

   def auth_user
     token = params[:token]
     auth = AuthenticationToken.find_by(body: token) 
     if auth
       self.current_user = auth.user
     else
       render json: {code: 403, errors: [I18n.t('system.login.messages.error.auth')].to_json,  message: I18n.t('system.login.messages.error.auth')}, status: 403; return
     end
   end

   def roles parm
     current_user.roles.each do |role|
       if role.code == parm
         return true 
       end
     end
     return false
   end

end