class ApplicationController < ActionController::API
  include CurrentMethods
  skip_before_filter :verify_authenticity_token
  after_filter :allow_headers
  
  def allow_headers
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = '*'
  end


end
