class PaymentMailer < ApplicationMailer
  default from: Constant::FROM_EMAIL
  
  def error_save(error, obj_js)
     @error  = error
     @obj_js = obj_js
     mail(to: Constant::TO_EMAIL, subject: 'Error en una transaccion de pago')
  end

end
