# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'grapi'

#aqui puede ir la llave que usas para entrar en bitbuket
set :repo_url, 'https://jrojas16410034:password@bitbucket.org/globalr/grapi.git'

#Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/deployer/grapi'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :default_env, { rvm_bin_path: '~/.rvm/bin' }

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do
  before :deploy, "deploy:stop"
 
  # start nginx
  after :deploy, "deploy:start"

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :sudo, "./restart.sh"
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

end
