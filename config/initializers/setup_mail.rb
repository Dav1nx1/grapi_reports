Rails.application.configure do
 config.action_mailer.delivery_method = :smtp
 config.action_mailer.smtp_settings = {
  :address              => Constant::MAILSETTING[:address],
  :port                 => Constant::MAILSETTING[:port],
  :domain               => Constant::MAILSETTING[:domain],
  :user_name            => Constant::MAILSETTING[:user_name],
  :password             => Constant::MAILSETTING[:password],
  :authentication       => Constant::MAILSETTING[:authentication],
  :enable_starttls_auto => Constant::MAILSETTING[:enable_starttls_auto]
 }
end