Rails.application.routes.draw do


  namespace :api, path: '/', constraints: { subdomain: Constant::SUBDOMAIN} do

    namespace :home do
      namespace :v1 do
        get 'welcome/index'
      end
    end

    namespace :profile do
      namespace :v1 do

        devise_for :users, only: [:sessions], defaults: { format: :json }
        resources  :users, defaults: { format: :json } do
           collection do
              get  'active_account/:active_token' => 'users#active_account'
              post 'change_pass/:token_pass' => 'users#change_pass'
              post :forgot_pass
           end
        end
        resources  :commerces, defaults: { format: :json }

      end
    end

    namespace :enterprise do
      namespace :v1 do
        resources :payments, only: [:create, :index, :show], defaults: { format: :json } do
          collection do
              get 'transaction/:order_num' => 'payments#transaction'
          end
        end
      end
    end

    namespace :oc do
      namespace :v1 do
        resources :transacciones, only: [:index, :show], defaults: { format: :json } do
          collection do
            get 'today' => 'transacciones#today'
            get 'by_sport_center/:sport_center' => 'transacciones#by_sport_center'
          end
        end
        resources :centros, only: [:index, :show], defaults: { format: :json }
      end
    end

    namespace :human do
      namespace :v1 do
        resources :names, only: [:index, :show], defaults: { format: :json }
      end
    end

    namespace :math do
      namespace :v1 do
        resources :numbers, only: [:index], defaults: { format: :json } do
          collection do
            get 'digit' => 'numbers#digit'
            get 'int' => 'numbers#integer'
            get 'int/:size' => 'numbers#integer'
            get 'int/:size/:quantity' => 'numbers#integer'
            get 'integer' => 'numbers#integer'
            get 'integer/:size' => 'numbers#integer'
            get 'integer/:size/:quantity' => 'numbers#integer'
            get 'dec' => 'numbers#decimal'
            get 'dec/:size' => 'numbers#decimal'
            get 'dec/:size/:quantity' => 'numbers#decimal'
            get 'decimal' => 'numbers#decimal'
            get 'decimal/:size' => 'numbers#decimal'
            get 'decimal/:size/:quantity' => 'numbers#decimal'
            get 'hex' => 'numbers#hexadecimal'
            get 'hex/:size' => 'numbers#hexadecimal'
            get 'hex/:size/:quantity' => 'numbers#hexadecimal'
            get 'hexadecimal' => 'numbers#hexadecimal'
            get 'hexadecimal/:size' => 'numbers#hexadecimal'
            get 'hexadecimal/:size/:quantity' => 'numbers#hexadecimal'
          end
        end
      end
    end

    namespace :address do
      namespace :v1 do
        resources :countries, only: [:index, :show] , defaults: { format: :json } do
          resources :states, only: [:index] , defaults: { format: :json }
        end
      end
    end

    namespace :setting do
      namespace :v1 do
        resources :points, only: [:index] , defaults: { format: :json }
      end
    end

    #only test for instanpagos
    resources :test_payments, only: [:index] do
      collection do
        get :insta_pay
        get :total_transactions
        get :total_center
      end
    end

   end
end
