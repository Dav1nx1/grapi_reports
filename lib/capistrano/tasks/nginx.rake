namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} nginx"
    task command.intern do
      on roles (:web) do
        execute "sudo service nginx #{command}"
      end
    end
  end
end