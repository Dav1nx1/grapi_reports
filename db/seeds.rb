# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require './db/seeds/countries.rb'
require './db/seeds/states_venezuela.rb'
require './db/seeds/status_entities.rb'
require './db/seeds/roles.rb'
require './db/seeds/settins.rb'
require './db/seeds/points.rb'
require './db/seeds/users.rb'
