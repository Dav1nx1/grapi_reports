# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160125152220) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "authentication_tokens", force: :cascade do |t|
    t.string   "body"
    t.integer  "user_id"
    t.datetime "last_used_at"
    t.string   "ip_address"
    t.string   "user_agent"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "authentication_tokens", ["user_id"], name: "index_authentication_tokens_on_user_id", using: :btree

  create_table "commerces", force: :cascade do |t|
    t.uuid     "uuid"
    t.integer  "user_id",                                   null: false
    t.integer  "status_entity_id",                          null: false
    t.integer  "country_id",                                null: false
    t.integer  "setting_id",                                null: false
    t.string   "public_key",                                null: false
    t.string   "name",             limit: 50,  default: "", null: false
    t.string   "web",              limit: 200, default: "", null: false
    t.string   "mobile_phone",     limit: 15,  default: "", null: false
    t.string   "local_phone",      limit: 15,  default: "", null: false
    t.text     "note"
    t.string   "logo",             limit: 200, default: "", null: false
    t.integer  "created_by",                                null: false
    t.integer  "updated_by",                                null: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "commerces", ["country_id"], name: "index_commerces_on_country_id", using: :btree
  add_index "commerces", ["public_key"], name: "index_commerces_on_public_key", using: :btree
  add_index "commerces", ["setting_id"], name: "index_commerces_on_setting_id", using: :btree
  add_index "commerces", ["status_entity_id"], name: "index_commerces_on_status_entity_id", using: :btree
  add_index "commerces", ["user_id"], name: "index_commerces_on_user_id", using: :btree

  create_table "commerces_points", force: :cascade do |t|
    t.integer  "commerce_id",      null: false
    t.integer  "point_id",         null: false
    t.integer  "status_entity_id", null: false
    t.string   "secret_key",       null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "commerces_points", ["commerce_id"], name: "index_commerces_points_on_commerce_id", using: :btree
  add_index "commerces_points", ["point_id"], name: "index_commerces_points_on_point_id", using: :btree
  add_index "commerces_points", ["secret_key"], name: "index_commerces_points_on_secret_key", using: :btree
  add_index "commerces_points", ["status_entity_id"], name: "index_commerces_points_on_status_entity_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "code",       limit: 5,  default: "", null: false
    t.string   "name",       limit: 60, default: "", null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id",                       null: false
    t.integer  "commerce_id",                   null: false
    t.integer  "point_id",                      null: false
    t.integer  "status_entity_id",              null: false
    t.string   "card_holder",                   null: false
    t.string   "email",                         null: false
    t.float    "amount",                        null: false
    t.string   "card_holder_id",                null: false
    t.string   "card_number",                   null: false
    t.string   "order_number",                  null: false
    t.string   "address",                       null: false
    t.string   "city",             default: ""
    t.string   "zip_code",         default: ""
    t.string   "state",            default: ""
    t.string   "description"
    t.string   "transaction_id"
    t.string   "reference"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "card_name"
    t.text     "result"
    t.integer  "code",             default: 0
    t.string   "responsecode"
    t.string   "message"
  end

  add_index "payments", ["commerce_id"], name: "index_payments_on_commerce_id", using: :btree
  add_index "payments", ["point_id"], name: "index_payments_on_point_id", using: :btree
  add_index "payments", ["status_entity_id"], name: "index_payments_on_status_entity_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "points", force: :cascade do |t|
    t.integer  "status_entity_id",                            null: false
    t.string   "short_name",          limit: 5,  default: "", null: false
    t.string   "name",                limit: 50, default: "", null: false
    t.string   "internal_public_key",            default: "", null: false
    t.string   "internal_secret_key",            default: "", null: false
    t.string   "url_pay",                        default: "", null: false
    t.string   "method_pay",                     default: "", null: false
    t.integer  "created_by",                                  null: false
    t.integer  "updated_by",                                  null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "points", ["status_entity_id"], name: "index_points_on_status_entity_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.integer  "status_entity_id",                         null: false
    t.string   "code",             limit: 5,  default: "", null: false
    t.string   "name",             limit: 30, default: "", null: false
    t.integer  "created_by",                               null: false
    t.integer  "updated_by",                               null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "roles", ["status_entity_id"], name: "index_roles_on_status_entity_id", using: :btree

  create_table "roles_users", force: :cascade do |t|
    t.integer  "role_id",          null: false
    t.integer  "user_id",          null: false
    t.integer  "status_entity_id", null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "roles_users", ["role_id"], name: "index_roles_users_on_role_id", using: :btree
  add_index "roles_users", ["status_entity_id"], name: "index_roles_users_on_status_entity_id", using: :btree
  add_index "roles_users", ["user_id"], name: "index_roles_users_on_user_id", using: :btree

  create_table "settings", force: :cascade do |t|
    t.integer  "status_entity_id",                         null: false
    t.integer  "country_id",                               null: false
    t.float    "tax",                        default: 0.0
    t.float    "commission",                 default: 0.0
    t.string   "currency",                   default: "0"
    t.string   "symbol_currency",  limit: 4
    t.integer  "created_by",                               null: false
    t.integer  "updated_by",                               null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "settings", ["country_id"], name: "index_settings_on_country_id", using: :btree
  add_index "settings", ["status_entity_id"], name: "index_settings_on_status_entity_id", using: :btree

  create_table "states", force: :cascade do |t|
    t.integer  "country_id",                              null: false
    t.string   "code_iso_3166_2", limit: 5,  default: "", null: false
    t.string   "name",            limit: 40, default: "", null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "status_entities", force: :cascade do |t|
    t.string   "code",       limit: 5,  default: "", null: false
    t.string   "name",       limit: 30, default: "", null: false
    t.integer  "created_by",                         null: false
    t.integer  "updated_by",                         null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "status_entity_id",                                null: false
    t.uuid     "uuid"
    t.string   "first_name",             limit: 50,  default: "", null: false
    t.string   "last_name",              limit: 50,  default: "", null: false
    t.string   "email",                  limit: 50,  default: "", null: false
    t.string   "user_tag",               limit: 50,               null: false
    t.string   "avatar"
    t.string   "country",                limit: 50
    t.string   "state",                  limit: 50
    t.string   "city",                   limit: 50
    t.integer  "zip_code",               limit: 8,   default: 0
    t.string   "street_name",            limit: 100
    t.string   "street_num",             limit: 10
    t.string   "address",                limit: 300
    t.string   "home_phone",             limit: 15
    t.string   "mobile_phone",           limit: 15
    t.string   "active_token"
    t.string   "encrypted_password",                 default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "created_by",                         default: 0,  null: false
    t.integer  "updated_by",                         default: 0,  null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "token_pass"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["status_entity_id"], name: "index_users_on_status_entity_id", using: :btree

  add_foreign_key "authentication_tokens", "users"
end
