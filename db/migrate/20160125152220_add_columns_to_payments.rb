class AddColumnsToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :code, :integer, default: 0
    add_column :payments, :responsecode, :string
    add_column :payments, :message, :string
  end
end
