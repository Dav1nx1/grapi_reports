class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string     :code, limit: 5, null: false, default: ''
      t.string     :name, limit: 60, null: false, default: ''
      t.timestamps null: false
    end
  end
end
