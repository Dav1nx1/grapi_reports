class CreateRolesUsers < ActiveRecord::Migration
  def change
    create_table :roles_users do |t|
      t.references :role, index: true, null: false
      t.references :user, index: true, null: false
      t.references :status_entity, index: true, null: false
      t.timestamps null: false
    end
  end
end
