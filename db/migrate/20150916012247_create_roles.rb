class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.references :status_entity, index: true, null: false
      t.string    :code, limit: 5, null: false, default: ''
      t.string    :name, limit: 30, null: false, default: ''
      t.integer    :created_by, null: false
      t.integer    :updated_by, null: false
      t.timestamps null: false
    end
  end
end
