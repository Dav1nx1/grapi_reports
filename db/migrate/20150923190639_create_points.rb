class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.references  :status_entity, index: true, null: false
      t.string      :short_name, limit: 5, null: false, default: ''
      t.string      :name, limit: 50, null: false, default: ''
      t.string      :internal_public_key, null: false, default: ''
      t.string      :internal_secret_key, null: false, default: ''
      t.string      :url_pay, null: false, default: ''
      t.string      :method_pay, null: false, default: ''
      t.integer     :created_by, null: false
      t.integer     :updated_by, null: false
      t.timestamps  null: false
    end
  end
end
