class AddNameCardToPayments < ActiveRecord::Migration
  def change
  	 add_column :payments, :card_name, :string, default: nil
  end
end
