class AddTokenPasswordToUsers < ActiveRecord::Migration
  def change
  	 add_column :users, :token_pass, :string, default: nil, index: true
  end
end
