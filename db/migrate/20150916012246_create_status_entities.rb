class CreateStatusEntities < ActiveRecord::Migration
  def change
    create_table :status_entities do |t|
      t.string     :code, limit: 5, null: false, default: ''
      t.string     :name, limit: 30, null: false, default: ''
      t.integer    :created_by, null: false
      t.integer    :updated_by, null: false
      t.timestamps null: false
    end
  end
end
