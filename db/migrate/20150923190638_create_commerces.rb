class CreateCommerces < ActiveRecord::Migration
  enable_extension 'uuid-ossp'
  def change
    create_table :commerces do |t|
      t.uuid        :uuid
      t.references  :user, index: true, null: false
      t.references  :status_entity, index: true, null: false
      t.references  :country, index: true, null: false
      t.references  :setting, index: true, null: false
      t.string      :public_key, index: true, null: false
      t.string      :name, limit: 50, null: false, default: ''
      t.string      :web, limit: 200, null: false, default: ''
      t.string      :mobile_phone, limit: 15, null: false, default: ''
      t.string      :local_phone, limit: 15, null: false, default: ''
      t.text        :note
      t.string      :logo, limit: 200, null: false, default: ''
      t.integer     :created_by, null: false
      t.integer     :updated_by, null: false
      t.timestamps   null: false
    end
  end
end
