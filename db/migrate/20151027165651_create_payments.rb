class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references  :user, index: true, null: false
      t.references  :commerce, index: true, null: false
      t.references  :point, index: true, null: false
      t.references  :status_entity, index: true, null: false
      t.string      :card_holder, null: false
      t.string      :email, null: false
      t.float       :amount, null: false
      t.string      :card_holder_id, null: false
      t.string      :card_number, null: false
      t.integer     :security_code, null: false
      t.string      :order_number, null: false
      t.string      :address, default: ''
      t.string      :city, default: ''
      t.string      :zip_code, default: ''
      t.string      :state, default: ''
      t.string      :description
      t.string      :transaction_id
      t.string      :expiration_date
      t.string      :reference
      t.timestamps  null: false
    end
  end
end
