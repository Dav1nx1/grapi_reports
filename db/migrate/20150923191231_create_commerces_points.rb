class CreateCommercesPoints < ActiveRecord::Migration
  def change
    create_table :commerces_points do |t|
      t.references  :commerce, index: true, null: false
      t.references  :point, index: true, null: false
      t.references  :status_entity, index: true, null: false
      t.string      :secret_key, index: true, null: false
      t.timestamps  null: false
    end
  end
end
