class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.references  :status_entity, index: true, null: false
      t.references  :country, index: true, null: false
      t.float       :tax, default: 0
      t.float       :commission, default: 0
      t.string      :currency, default: 0
      t.string      :symbol_currency, limit: 4
      t.integer     :created_by, null: false
      t.integer     :updated_by, null: false
      t.timestamps  null: false
    end
  end
end
