class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.references :country, index: true, null: false
      t.string     :code_iso_3166_2, limit: 5, null: false, default: ''
      t.string     :name, limit: 40, null: false, default: ''
      t.timestamps null: false
    end
  end
end
