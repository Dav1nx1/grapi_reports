class RemoveSecurityCodeToPayments < ActiveRecord::Migration
  def change
  	 remove_column :payments, :security_code
  	 remove_column :payments, :expiration_date
  end
end