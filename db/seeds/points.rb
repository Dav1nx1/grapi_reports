Point.destroy_all
Point.create(
  [
    {
     status_entity_id: StatusEntity.find_by(code: 'ACT').id, 
     short_name: 'INTP' ,
     name: 'Instapagos Banesco', 
     internal_public_key: '1783b25bca451cc74f90a776838ab242', 
     internal_secret_key: '1B2398F2-1B8C-4BA2-B3BF-A779F589D77E', 
     url_pay: 'https://api.instapago.com/payment',
     method_pay: 'insta_pay',
     created_by: 0, 
     updated_by:0
    },
    {
     status_entity_id: StatusEntity.find_by(code: 'ACT').id, 
     short_name: 'INTPM' ,
     name: 'Instapagos Banesco Mobile', 
     internal_public_key: '1783b25bca451cc74f90a776838ab242', 
     internal_secret_key: '1B2398F2-1B8C-4BA2-B3BF-A779F589D77E', 
     url_pay: 'https://api.instapago.com/payment',
     method_pay: 'insta_pay',
     created_by: 0, 
     updated_by:0
    }
  ])