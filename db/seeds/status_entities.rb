StatusEntity.destroy_all
StatusEntity.create(
  [
    { name: 'Active', code: 'ACT', created_by: 0, updated_by: 0},
    { name: 'Inactive', code: 'IACT', created_by: 0, updated_by: 0},
    { name: 'Deleted', code: 'DEL', created_by: 0, updated_by: 0},
    { name: 'Processed', code: 'PROCD', created_by: 0, updated_by: 0},
    { name: 'Standby', code: 'STB', created_by: 0, updated_by: 0},
    { name: 'Rejected', code: 'REJD', created_by: 0, updated_by: 0}
  ])