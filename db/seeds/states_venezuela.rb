
country = Country.find_by(name: 'Venezuela')
State.destroy_all
State.create(
  [
    { name: 'Amazonas', code_iso_3166_2: 'VE-X', country_id: country.id },
    { name: 'Anzoátegui', code_iso_3166_2: 'VE-B', country_id: country.id },
    { name: 'Apure', code_iso_3166_2: 'VE-C', country_id: country.id },
    { name: 'Aragua', code_iso_3166_2: 'VE-D', country_id: country.id },
    { name: 'Barinas', code_iso_3166_2: 'VE-E', country_id: country.id },
    { name: 'Bolívar', code_iso_3166_2: 'VE-F', country_id: country.id },
    { name: 'Carabobo', code_iso_3166_2: 'VE-G', country_id: country.id },
    { name: 'Cojedes', code_iso_3166_2: 'VE-H', country_id: country.id },
    { name: 'Delta Amacuro', code_iso_3166_2: 'VE-Y', country_id: country.id },
    { name: 'Falcón', code_iso_3166_2: 'VE-I', country_id: country.id },
    { name: 'Guárico', code_iso_3166_2: 'VE-J', country_id: country.id },
    { name: 'Lara', code_iso_3166_2: 'VE-K', country_id: country.id },
    { name: 'Mérida', code_iso_3166_2: 'VE-L', country_id: country.id },
    { name: 'Miranda', code_iso_3166_2: 'VE-M', country_id: country.id },
    { name: 'Monagas', code_iso_3166_2: 'VE-N', country_id: country.id },
    { name: 'Nueva Esparta', code_iso_3166_2: 'VE-O', country_id: country.id },
    { name: 'Portuguesa', code_iso_3166_2: 'VE-P', country_id: country.id },
    { name: 'Sucre', code_iso_3166_2: 'VE-R', country_id: country.id },
    { name: 'Táchira', code_iso_3166_2: 'VE-S', country_id: country.id },
    { name: 'Trujillo', code_iso_3166_2: 'VE-T', country_id: country.id },
    { name: 'Vargas', code_iso_3166_2: 'VE-W', country_id: country.id },
    { name: 'Yaracuy', code_iso_3166_2: 'VE-U', country_id: country.id },
    { name: 'Zulia', code_iso_3166_2: 'VE-V', country_id: country.id },
    { name: 'Distrito Capital', code_iso_3166_2: 'VE-A', country_id: country.id },
    { name: 'Dependencias Federales', code_iso_3166_2: 'VE-Z', country_id: country.id }
  ])